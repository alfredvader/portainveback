//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bcrypt = require('bcrypt');
var saltRounds = 10;
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
  next();
})

const config = require('./config')
const apipubCtrl = require('./apipub/apipub')
var path = require('path');
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/alazcano/collections";
var requestjson = require('request-json');
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

app.listen(port);

//Peticion REST para Login
app.post('/Login', function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  //var email = req.headers.email;
  //var password = req.headers.password;
  var email = req.body.email;
  var password = req.body.password;

  //var query = 'q={"email":"'+ email +'", "password":"' + password + '"}';
  var query = 'q={"email":"'+ email +'"}';
  console.log(query);
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body) {
   if (!err) {
     if (body.length > 0) {
      console.log('sin encriptar pass ' + password + ' Otro password en BD ' + body[0].password);
      bcrypt.compare(password, body[0].password, function(err, ok) {
        if (ok) {
          res.status(200).send('Usuario logado correctamente');
          console.log("ok")
        } else {
          res.status(404).send('Password no encontrado');
          console.log("No")
        }
       });
     } else {
      res.status(404).send('Usuario no encontrado');
      console.log("No Encontrado")
    }

   } else {
     res.status(404).send('Error en lo datos')
     console.log("Error de body");
   }
  })
})

 //Inserta en Collection Usuarios
app.post('/GuardaUser', function(req, res) {
  console.log("Recibida la peticion=> " + urlMLabRaiz + "/Usuarios?" + apiKey)
   clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey);
   bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
     req.body.password = hash;
     clienteMLabRaiz.post('', req.body, function(err,resM, body) {
        if (err) {
          console.log("Error encriptando" + body); }
        else {
          res.send(body);
        }
      })
   });
 })

//Inserta inversiones de Fondos
app.post('/Invierte', function(req, res) {
   clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Inversion?" + apiKey);
   //console.log(urlMLabRaiz + "/Inversion?" + apiKey);
   clienteMLabRaiz.post('', req.body, function(err,resM, body) {
     if (err) {
       console.log(body); }
     else {
       res.send(body);
     }
   })
})

 //Peticion GET para obtener el saldo
app.post('/Saldo', function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  //var email = req.headers.email;
  var email = req.body.email;
  var query = 'q={"email":"'+ email + '"}';
  console.log("se ha pedido saldo de " + query)
  var clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Inversion?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body) {
    if (err) {
      console.log(body); }
    else {
      res.send(body);
      console.log("regresa datos" + res)
      }
    })
})

app.get('/apipubget',function(req,res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
  var now = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('T')[0];
  var serieBancoMex = config.url_bmx +'/'+ config.serie_TC +',' + config.serie_TIIE +'/datos/' + now + '/' + now;
  console.log("fecha "+ now + serieBancoMex);
  apipubCtrl.getTipoCam(serieBancoMex)
  .then( data => res.send(data))
  .catch(error => console.error(error))
})
