'use strict'
const request = require('request');
const fetch = require('node-fetch');
global.fetch = fetch;
global.Headers = fetch.Headers;

var req = require("../node_modules/request")
const config = require('../config');

function getTipoCam(url) {
  return fetch(url, {
    credentials: 'same-origin',
    method: 'GET',
    headers: new Headers({
      'Bmx-Token': config.BmxToken
    }),
  })
  .then(response => response.json())
}

module.exports = {
  getTipoCam
}
